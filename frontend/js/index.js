class App {
    constructor() {
        this.initWebSocket();

        // Set svg attributes
        this.margin= ({top: 20, right: 0, bottom: 30, left: 40});
        this.categoryIndent = 4*15 + 5;

        // Set up current location visualization
        this.cv = d3.select("#cv");
        this.cvHeight = Number(this.cv.style("height").slice(0, -2));
        this.cvWidth = Number(this.cv.style("width").slice(0, -2));
        this.cvSvg = this.cv.append("svg");
        this.setWindowSize(this.cvSvg, this.cvHeight, this.cvWidth);

        // Set up current location visualization
        this.dv = d3.select("#dv");
        this.dvHeight = Number(this.dv.style("height").slice(0, -2));
        this.dvWidth = Number(this.dv.style("width").slice(0, -2));
        this.dvSvg = this.dv.append("svg");
        this.setWindowSize(this.dvSvg, this.dvHeight, this.dvWidth);
        this.dvSvg = this.dvSvg.append("g")
                         .attr("transform", "translate(" + this.margin.left + ", 0)");
        
        this.dvWidth -= this.margin.left - this.margin.right;
        this.dvHeight -= this.margin.top - this.margin.bottom;
        let defaultBarWidth = 2000;

        //Set up scales
        this.x = d3.scaleLinear()
            .domain([0,defaultBarWidth])
            .range([0,this.dvWidth - this.margin.left -this.margin.right]);
        this.y = d3.scaleBand()
            .range([0, this.dvHeight - this.margin.top - this.margin.bottom])
            .padding(0.1);
    }

    // Initiate websocket
    initWebSocket() {
        if (!this.webSocket) {
            this.webSocket = new WebSocket("ws://localhost:8888");
            this.webSocket.onclose = this.onClose(this);
            this.webSocket.onopen = this.onOpen(this);
            this.webSocket.onerror = this.onError(this);
            this.webSocket.onmessage = this.onMessage(this);
        }
    }

    // Webscoket callback function
    onOpen(ctr) {
        return () => {
            console.log("Websocket connected...");
            ctr.webSocket.send("request data");
        }
    }

    // Webscoket callback function
    onClose(ctr) {
        return () => {
            console.log("Websocket closed...");
        }
    }

    // Webscoket callback function
    onError(ctr) {
        return (error) => {
            console.error(error);
        }
    }

    // Webscoket callback function
    onMessage(ctr) {
        return (message) => {
            // Parse json to object
            let data = message.data;
            let carRecords = JSON.parse(data);

            // Render
            ctr.drawBarChart(carRecords.data.sort((a, b) => {
                return a.distance > b.distance;
            }));
            ctr.drawLocation(carRecords.data);
        }
    }

    // Set svg size
    setWindowSize(target, height, width) {
        target.attr("height", height)
              .attr("width", width);
    }

    // Render bar chart
    drawBarChart(newdata) {
        // Import settings
        let margin=this.margin, 
            width=this.dvWidth, 
            height=this.dvHeight, 
            categoryIndent=this.categoryIndent, 
            svg=this.dvSvg, x=this.x, y=this.y;

        // Reset domains
        y.domain(newdata.sort((a,b) => {
                return b.distance - a.distance;
            })
            .map(d => d.carId)
        );
        
        let barmax = d3.max(newdata, d=> d.distance);
        
        x.domain([0, barmax]);

	    // Bind new data to chart rows 
	    // Create chart row and move to below the bottom of the chart
	    let chartRow = svg.selectAll("g.chartRow")
	                    .data(newdata, d => d.carId);
	    let newRow = chartRow
                        .enter()
                        .append("g")
                        .attr("class", "chartRow")
                        .attr("key", (d) => d.carId)
                        .attr("transform", "translate(0," + (height + margin.top + margin.bottom) + ")");

        // Add rectangles
        newRow.insert("rect")
                .attr("class","bar")
                .attr("x", 0)
                .attr("opacity", 1)
                .attr("height", 30)
                .attr("width", d => x(d.distance));

        // Add value labels
        newRow.append("text")
                .attr("class","label")
                .attr("y", (y.bandwidth()-25)/2)
                .attr("x",0)
                .attr("opacity",0)
                .attr("dy",".35em")
                .attr("dx","0.5em")
                .text(d => d.distance); 
	
        // Add Headlines
        newRow.append("text")
                .attr("class","category")
                .attr("text-overflow","ellipsis")
                .attr("y", (y.bandwidth()-25)/2)
                .attr("x",categoryIndent)
                .attr("opacity",0)
                .attr("dy",".35em")
                .attr("dx","0.5em")
                .text(d => {
                    return "Car ID: "+d.carId
                });
        
        // Update bar widths
        chartRow.select(".bar")
                .transition()
                .duration(300)
                .attr("width", d => x(d.distance))
                .attr("opacity",1);

        // Update data labels
        chartRow.select(".label").transition()
                .duration(300)
                .attr("opacity",1)
                .tween("text", (d, ind, n) => { 
                    let i = d3.interpolate(+n[ind].textContent.replace(/\,/g,''), +d.distance);
                    return (t) => {
                            return n[ind].textContent = Math.round(i(t));
                        };
                });

        // Fade in categories
        chartRow.select(".category").transition()
                .duration(300)
                .attr("opacity",1);

        // Fade out and remove exit elements
        chartRow
                .exit().transition()
                .style("opacity", "0")
                .attr("transform", (d) => {
                    return `translate(0, ${(height + margin.top + margin.bottom)})`;
                })
                .remove();

        // Animation of bar movement
        setTimeout(() => {
            d3.selectAll(".chartRow")
                .transition()
                .delay((d, i) => i * 30)
                .duration(900)
                .attr("transform", (d) => { 
                    return "translate(0," + (y(d.carId) + y.bandwidth()/2) + ")"; 
                });
        }, 300)
    }

    // Render location graph
    drawLocation(data) {
        let svg = this.cvSvg,
            margin = this.margin,
            height = this.cvHeight,
            width = this.cvWidth,
            bottom = 30;

        // Clear all element
        let cv = d3.select("#cv").selectAll("svg > *").remove();

        // Set x, y scale for x axis and y axis
        let x = d3.scaleBand()
            .domain(data.map(d => d.carId))
            .range([margin.left, width - margin.right])
            .padding(0.1),

            y = d3.scaleLinear()
            .domain([0, d3.max(data, d => d.location)]).nice()
            .range([height - margin.bottom - bottom, margin.top]),
            
            colorScale = d3.scaleOrdinal(d3["schemeCategory10"]),

            xAxis = g => g
                    .attr("transform", `translate(0,${height - margin.bottom -bottom})`)
                    .call(d3.axisBottom(x)
                    .tickSizeOuter(0)),

            yAxis = g => g
                    .attr("transform", `translate(${margin.left},0)`)
                    .call(d3.axisLeft(y))
                    .call(g => g.select(".domain").remove());

        // Render data
        let circleg = svg.selectAll("g").data(data).enter().append("g");
        // Add circle
        circleg.append("circle")
                .attr("cx", d => x(d.carId)+x.bandwidth()/2)
                .attr("cy", d => y(d.location))
                .attr("r", 5)
                .attr("fill", (d, i) => colorScale(i) )
        // Add text
        circleg.append("text")
            .attr("class","location-id")
            .attr("text-overflow","ellipsis")
            .attr("y", d => y(d.location)+10)
            .attr("x", d => x(d.carId))
            .attr("opacity",1)
            .attr("dy",".1em")
            .attr("dx","0.5em")
            .text(function(d){
                return "Car ID: "+d.carId
            });

        // Add x axis
        svg.append("g")
           .call(xAxis)
           .selectAll("text")
           .attr("y", 9)
           .attr("x", 0)
           .attr("dy", ".35em")
           .attr("transform", "rotate(45)")
           .style("text-anchor", "start");
        // Add y axis
        svg.append("g")
           .call(yAxis);
    }
}

var app = new App()