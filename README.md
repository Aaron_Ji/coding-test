Code Test
=========
[![node(scoped)](https://img.shields.io/node/v/@stdlib/stdlib.svg)](https://github.com/nodejs/node)
[![mocha(scoped)](https://img.shields.io/badge/mocha-5.2.0-brightgreen.svg)](https://github.com/mochajs/mocha)
[![chai(scoped)](https://img.shields.io/badge/chai-4.2.0-brightgreen.svg)](https://github.com/chaijs/chai)
[![nodemon(scoped)](https://img.shields.io/badge/nodemon-1.18.4-brightgreen.svg)](https://github.com/remy/nodemon)
[![supertest(scoped)](https://img.shields.io/badge/supertest-3.3.0-brightgreen.svg)](https://github.com/visionmedia/supertest)
[![websocket(scoped)](https://img.shields.io/badge/websocket-1.0.28-brightgreen.svg)](https://github.com/theturtle32/WebSocket-Node)
[![D3.js(scoped)](https://img.shields.io/badge/d3.js-5.7.0-brightgreen.svg)](https://github.com/d3/d3)

## Run project

-----------

Install node js

Clone project

```bash
$ git clone https://Aaron_Ji@bitbucket.org/Aaron_Ji/coding-test.git
$ cd coding-test
```

Initiate project

```bash
$ npm install
```

Run project, the server will be hosted at http://localhost:8888
```bash
$ npm start
```

## ER Graph
------------
![ER Graph](./screenshots/ER_Graph.jpg)

## Test RESTful API
------------
Test user apis
```bash
$ mocha ./backend/test/api-test/UserAPI.test.js
```

Test car apis
```bash
$ mocha ./backend/test/api-test/CarAPI.test.js
```

Test demand apis
```bash
$ mocha ./backend/test/api-test/DemandAPI.test.js
```

Test schedule apis
```bash
$ mocha ./backend/test/api-test/ScheduleAPI.test.js
```

## Dashboard
------------
![Dashboard](./screenshots/dashboard.gif)
