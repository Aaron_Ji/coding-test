const Demand = require("../model/demand");
const { TargetNotFoundError } = require("../errors/errors");
const UserController = require("./UserController");

class DemandController {
    constructor() {
        this.demands = new Map();
    }

    /**
     * Singleton instance
     */
    static getInstance() {
        if (!this.demandController) {
            this.demandController = new DemandController();
            // this.demandController.userController = UserController.getInstance();
        }

        return this.demandController;
    }

    // Only for test
    testAdd(demand) {
        if (this.demands.size == 0) {
            this.default = demand;
        }
        demand.demandId = (this.demands.size+1).toString();
        this.addDemand(demand);
    }

    /**
     * Get all demands
     */
    getDemands() {
        return Array.from(this.demands.values());
    }

    /**
     * Get demands by id
     * @param {demand id} id 
     */
    getDemandById(id) {
        let demand = this.demands.get(id);
        if (demand === undefined) {
            throw new TargetNotFoundError("Get demand failed", `Cannot find demand by id ${id}`);
        }
        return demand;
    }

    /**
     * Add demand
     * @param {instance of Demand} demand 
     */
    addDemand(demand) {
        
        if (!UserController.isUserExisted(demand.UserId)) {
            throw new TargetNotFoundError("Add demand failed", `User ${demand.UserId} doesn't exist`);
        }
        this.demands.set(demand.demandId, demand);
        return demand;
    }

    /**
     * Update demand
     * @param {demand id} id 
     * @param {demand features: object} features 
     */
    updateDemandById(id, features) {
        let demand = this.getDemandById(id);

        if (demand === undefined) {
            throw new TargetNotFoundError("Update demand failed", `Cannot find demand by id ${id}`);
        }

        if ("userId" in features) {
            if (!UserController.isUserExisted(features.userId)) {
                throw new TargetNotFoundError("Add demand failed", `User ${features.userId} doesn't exist`);
            }
        }

        demand.setFeatures(features);
        this.demands.set(id, demand);
        return demand;
    }

    /**
     * Delete demand by id
     * @param {demand id} id 
     */
    deleteDemandById(id) {
        if (!this.demands.has(id)) {
            throw new TargetNotFoundError("Delete demand failed", `Cannot find demand by id ${id}`);
        }
        
        this.demands.delete(id);
    }

    /**
     * Check if there is a demand has user id
     * @param {user id} userId 
     */
    static isUserContained(userId) {
        for (let demand of this.demandController.demands.values()) {
            if (demand.UserId === userId) {
                return true
            }
        }
        return false;
    }
}

module.exports = DemandController;