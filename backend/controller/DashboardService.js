const Car = require("../model/car");
const carCtr = require("../controller/CarController").getInstance();

class DashboardService {
    constructor() {
        // Set attributes
        this.interval = null;

        // Record the previous location of cars
        this.previousCarLocation = carCtr.getCars();

        // Data includes carId, current location, overall distance of a car
        this.data = new Map();
        this.previousCarLocation.forEach(item => {
            this.data.set(item.carId, {
                carId: item.carId,
                location: item.currentLocation,
                distance: 0
            });
        });
    }

    /**
     * Singleton instance
     */
    static getInstance() {
        if (!this.dashboardService) {
            this.dashboardService = new DashboardService();
        }
        return this.dashboardService;
    }

    /**
     * Random update car location every 1.5s
     * Calculate needed data and invoke the send function
     * @param {callback function} fn 
     */
    getData(fn) {
        this.interval = setInterval(() => {
            let currentCarLocation = [];

            // Random car location
            this.previousCarLocation.forEach(item => {
                let newLocation = Math.floor(Math.random()*100);
                

                // Update distance
                let previousRecord = this.data.get(item.carId)
                let distance = previousRecord.distance + Math.abs(newLocation - item.currentLocation);
                previousRecord.location = newLocation,
                previousRecord.distance = distance
                this.data.set(item.carId, previousRecord);

                // Update location
                item.currentLocation = newLocation;
                currentCarLocation.push(item);

            });

            // Update previous records as the latest
            this.previousCarLocation = currentCarLocation;

            // Invoke the callback function
            fn(Array.from(this.data.values()));

        }, 1500)
        
    }

    /**
     * Stop the interval of random updating car locations
     */
    stop() {
        if (this.interval !== null) {
            clearInterval(this.interval);
        }
    }
}

module.exports = DashboardService;