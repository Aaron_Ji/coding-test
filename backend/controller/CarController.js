const Car = require("../model/car");
const {TargetNotFoundError} = require("../errors/errors");

class CarController {
    constructor() {
        this.cars = new Map();
    }

    /**
     * Singleton instance
     */
    static getInstance(){
        if (!this.carController) {
            this.carController = new CarController();
        }
        return this.carController;
    }

    // Only for test
    testAdd(car) {
        if (this.cars.size == 0) {
            this.default = car;
        }
        car.carId = (this.cars.size+1).toString();
        this.addCar(car);
    }

    /**
     * Get all cars
     */
    getCars() {
        return Array.from(this.cars.values());
    }

    /**
     * Get car by id
     * @param {car id} id 
     */
    getCarById(id) {
        let car = this.cars.get(id);
        if (car === undefined) {
            throw new TargetNotFoundError("Get car failed", `Cannot find car by id ${id}`);
        }
        return car;
    }

    /**
     * Add car
     * @param {instance of Car} car 
     */
    addCar(car) {
        this.cars.set(car.carId, car);
        return car;
    }

    /**
     * Update car features
     * @param {car id} id 
     * @param {car features: object} features 
     */
    updateCarById(id, features) {
        let car = this.getCarById(id);

        if (car === undefined) {
            throw new TargetNotFoundError("Update car failed", `Cannot find car by id ${id}`);
        }

        car.setFeatures(features);
        this.cars.set(id, car);
        return car;
    }

    /**
     * Delete car by id
     * @param {car id} id 
     */
    deleteCarById(id) {
        if (!this.cars.has(id)) {
            throw new TargetNotFoundError("Delete car failed", `Cannot find car by id ${id}`);
        }
        
        this.cars.delete(id);
    }
}

module.exports = CarController;