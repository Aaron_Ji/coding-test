class Utils {
    constructor() {}

    static getInstance() {
        if (!this.utils) {
            this.utils = new Utils();
        }

        return this.utils;
    }

    /**
     * Function for generating uuid
     * @param {length} len 
     * @param {code type} radix 
     */
    uuid(len, radix) {
        var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
        var uuid = [], i;
        radix = radix || chars.length;

        if (len) {
            // Compact form
            for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
        } else {
            // rfc4122, version 4 form
            var r;

            // rfc4122 requires these characters
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4';

            // Fill in random data. At i==19 set the high bits of clock sequence as
            // per rfc4122, sec. 4.1.5
            for (i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    r = 0 | Math.random()*16;
                    uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
                }
            }
        }

        return uuid.join('');
    }

    /**
     * Captialize the first letter of a string
     * @param {string} str 
     */
    captialize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    /**
     * Convert an object to string
     * @param {object} object 
     */
    toString(object) {
        if (object instanceof Array) {
            let itemStr = "";
            object.map((item) => {
                itemStr += this.toString(item) + ",";
            });
            
            return `[${itemStr.slice(0, itemStr.length-1)}]`;
        }
        if (object instanceof Object) {
            return this.getStringOfObject(object);
        }
    }

    /**
     * return the string of an object
     * @param {object} object 
     */
    getStringOfObject(object) {
        let items = "";
        let value = "";
        let type = "";
        for (let key in object) {
            value = object[key];
            type = typeof value;
            value = type === "number" || type === "boolean" ? 
                    value : type === "object" ? 
                    this.getStringOfObject(value) : `"${value}"`;
            items += `"${key}": ${value},`;
        }


        return `{${items.slice(0, items.length-1)}}`;
    }

    /**
     * Convert class instance to json object
     * @param {object} proto 
     */
    toJSON(proto) {
        let jsoned = {};
        let toConvert = proto || this;
        Object.getOwnPropertyNames(toConvert).forEach((prop) => {
            const val = toConvert[prop];

            // don't include those
            if (prop === 'toJSON' || prop === 'constructor') {
                return;
            }
            if (typeof val === 'function' || val === undefined) {
                // jsoned[prop] = val.bind(jsoned);
                return;
            }
            jsoned[prop] = val;
        });
    
        const inherited = Object.getPrototypeOf(toConvert);
        if (inherited !== null) {
            Object.keys(this.toJSON(inherited)).forEach(key => {
                if (!!jsoned[key] || key === 'constructor' || key === 'toJSON')
                    return;
                if (typeof inherited[key] === 'function' || inherited[key] === undefined) {
                    // jsoned[key] = inherited[key].bind(jsoned);
                    return;
                }
                jsoned[key] = inherited[key];
            });
        }
        return jsoned;
    }
    /**
     * Handle the error situation
     * @param {Http response for sending result back} response 
     * @param {JavaScript Error} error 
     * @param {Error code} code 
     */
    errorResponse(response, error, code) {

        response.writeHead(200, {'Content-Type': 'application/json'});
        let responseMessage = JSON.stringify({
            status: code,
            error: error.message,
            reason: error.reason
        });
        response.write(responseMessage);
        response.end();
    }

    /**
     * Handle success situation
     * @param {Http response} response 
     * @param {object} options 
     * @param {Http code} code 
     */
    successResponse(response, options, code) {
        try{
            response.writeHead(200, {'Content-Type': 'application/json'});
            let responseData = JSON.stringify({
                status: code || 200,
                data: options.data || {},
                message: options.message || 'success'
            });

            response.write(responseData);
            response.end();
        } catch (error) {
            this.errorResponse(response, new Error(`Server error: ${error.message}`), 500);
        }
    }
}

module.exports = Utils.getInstance();