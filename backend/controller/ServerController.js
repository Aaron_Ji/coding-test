const http = require("http");
const EventEmitter = require("events");
const utils = require("./utils");
const { URLError } = require("../errors/errors");

class ServerController extends EventEmitter{
    constructor() {
        super();
        this.eventGroups = {};
        this.SCHEMA = {
            POST: 'POST',
            PUT: 'PUT',
            GET: 'GET',
            DELETE: 'DELETE'
        }
    }

    static getInstance(){
        if (!this.serverController) {
            this.serverController = new ServerController();
        }
        return this.serverController;
    }

    // For HTTP GET operation
    get(urlPattern, fn) {
        // Parse the url pattern
        try {
            let eventName, params;
            if (urlPattern == "*") {
                // [eventname, params] = ["*", []];
                eventName = "*";
                params = [];
            } else {
                [eventName, params] = this.parseUrl(urlPattern, "/:", this.SCHEMA.GET);
            }

            this.on(eventName, (request, response, urlParams) => {

                request.params = this.mapUrlParams(params, urlParams);

                fn(request, response);
            }); 
        } catch (error) {
            console.error(error.message);
        }
    }

    // For HTTP PUT operation
    put(urlPattern, fn) {
        try {
            let [eventName, params] = this.parseUrl(urlPattern, "/:", this.SCHEMA.PUT);

            this.on(eventName, (request, response, urlParams, data) => {
                request.params = this.mapUrlParams(params, urlParams);
                request.body = data;
                fn(request, response);
            })
        } catch (error) {
            console.error(error.message);
        }
    }

    // For HTTP POST operation
    post(urlPattern, fn) {
        try {
            let [eventName, params] = this.parseUrl(urlPattern, "/:", this.SCHEMA.POST);
            
            this.on(eventName, (request, response, data) => {
                request.body = data;

                fn(request, response);
            })
        } catch (error) {
            console.error(error.message);
        }
    }

    // For HTTP DELETE operation
    delete(urlPattern, fn) {
        try {
            let [eventName, params] = this.parseUrl(urlPattern, "/:", this.SCHEMA.DELETE);

            this.on(eventName, (request, response, urlParams) => {
                request.params = this.mapUrlParams(params, urlParams);

                fn(request, response);
            });
        } catch (error) {
            console.error(error.message);
        }
    }

    // listen HTTP server
    listen(port, fn) {
        const server = http.createServer((request, response) => {
            try {
                let url = request.url;
                let eventName = null;
                let params = [];
                let body = "";
                let data = null;
                let group = null;
                let isMatched = false;

                // handle static files request
                if (url.indexOf(".") > 0) {
                    // console.log("here")
                    this.emit("*", request, response, []);
                    return ;
                }

                // Find the corresponding url pattern group
                for (let groupName in this.eventGroups) {
                    if (url.includes(groupName)) {
                        group = this.eventGroups[groupName];
                        params = this.extractParams(url.split(groupName)[1], "/");
                        eventName = `${groupName}-${params.length}`;

                        break;
                    }
                }

                if (eventName === null) {
                    throw new URLError(`Cannot ${request.method} ${request.url}`, "Invalid URL request");
                }

                switch(request.method) {

                    // Handle POST request
                    case this.SCHEMA.POST: 
                        eventName = `${this.SCHEMA.POST}-${eventName}`;

                        // Check if event exists
                        this.checkEvent(request, group, eventName);

                        // Invoke event
                        body = "";
                        data = null;
                        request.on("data", chunk => {
                            body += chunk.toString();
                        });

                        request.on("end", () => {
                            try{
                                data = JSON.parse(body.split("\n").join(""));
                                this.emit(eventName, request, response, data);
                            } catch (error) {
                                utils.errorResponse(response, error, 404);
                            }
                        });
                        break;

                    // Handle GET request
                    case this.SCHEMA.GET: 
                        eventName = `${this.SCHEMA.GET}-${eventName}`;

                        // Check if event exists
                        this.checkEvent(request, group, eventName);

                        // Invoke event
                        this.emit(eventName, request, response, params);
                        break;
                    
                    // Handle PUT request
                    case this.SCHEMA.PUT:
                        eventName = `${this.SCHEMA.PUT}-${eventName}`;

                        // Check if event exists
                        this.checkEvent(request, group, eventName);

                        // Convert data
                        body = "";
                        data = null;
                        request.on("data", chunk => {
                            body += chunk.toString();
                        });

                        // Invoke event
                        request.on("end", () => {
                            try{
                                data = JSON.parse(body.split("\n").join(""));
                                this.emit(eventName, request, response, params, data);
                            } catch (error) {
                                utils.errorResponse(response, error, 404);
                            }
                        });

                        break;
                    case this.SCHEMA.DELETE:
                        eventName = `${this.SCHEMA.DELETE}-${eventName}`;

                        // Check if event exists
                        this.checkEvent(request, group, eventName);

                        // Invoke event
                        this.emit(eventName, request, response, params);
                        
                        break;
                    
                    default: 
                        // response.end();
                        break;
                }
            } catch (error) {
                utils.errorResponse(response, error, 404);
            }
        });
        this.server = server;
        this.server.listen(port);
        fn();
    }

    /**
     * Check if the request is a api call and then parse the url,
     * extract the api type and parameters
     * @param {Url or url pattern} url 
     * @param {Term used to separate event name and parameters} locator
     * @param {Method of request method} schemaType 
     */
    parseUrl(urlPattern, locator, schemaType=null) {
    
        let locatorIndex = urlPattern.indexOf(locator);
        let groupName = "";
        let paramUrl = "";
        
        if (locatorIndex < 0) {
            // Url doesn't have parameters
            groupName = urlPattern;
        } else {
            // Separate url into groupName and paramUrl
            groupName = urlPattern.slice(0, locatorIndex);
            paramUrl = urlPattern.slice(locatorIndex);
        }

        let group = this.eventGroups[groupName] || {};
        let params = this.extractParams(paramUrl, locator);
        let eventName = `${groupName}-${params.length}`;
        
        if (schemaType !== null) {
            eventName = `${schemaType}-${eventName}`;
            group[eventName] = params;
            this.eventGroups[groupName] = group;
        } 

        return [eventName, params];
    }

    /**
     * Check if the invoked event exists or not
     * @param {Event name} eventName 
     */
    checkEvent(request, group, eventName) {
        if (!(eventName in group)) {
            throw new URLError(`Cannot ${request.method} ${request.url}`, "Invalid URL request");
        }
    }

    /**
     * Extract the substitude, like id from /api/users/:id,
     * Map the string array to an object array with term and its index
     * in the original array 
     * @param {array of params in url} params 
     * @param {Term used to separate parameters} locator
     */
    extractParams(url, locator) {
        let params = url.split(locator);
        params.shift();
        if (locator === "/:") {
            params = params.map((param, ind) => {
                let indexOfSlash = param.indexOf("/");
                if (indexOfSlash >= 0) {
                    param = param.slice(0, indexOfSlash);
                }
                return param;
            });
        }
        return params;
    } 

    /**
     * Extract the real value we want from the real url
     * @param {params extracted from the url pattern} params 
     * @param {params extracted from the real url} urlParams 
     */
    mapUrlParams(params, urlParams) {
        // Parse real url
        let realParams = {};

        // Map the params into an object, key is param name, value is the real value we want
        params.map((item, index) => {
            realParams[item] = urlParams[index];
        });

        return realParams;
    }

    get Server() {
        return this.server;
    }
}

module.exports = ServerController;