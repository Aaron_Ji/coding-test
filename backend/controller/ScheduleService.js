const DemandController = require("./DemandController");
const CarController = require("./CarController");
require("../test/add.data");

class ScheduleService {
    constructor() {
        // Get instance of controllers for demands and cars
        this.carCtr = CarController.getInstance();
        this.demandCtr = DemandController.getInstance();
    }

    /**
     * Singelton instance
     */
    static getInstance() {
        if (!this.service) {
            this.service = new ScheduleService()
        }
        return this.service;
    }
    
    /**
     * Get all available schedule according to current time
     * @param {timestamp} timestamp 
     */
    getCurrentSchedule(timestamp) {
        // Get current demands and cars
        let demands = this.demandCtr.getDemands(),
            cars = this.carCtr.getCars();

        // Initiate variables for caching data
        let carFeatureGroups = {
                                    carGroups: [],
                                    featureGroups: [],
                               },
            scheduleGroups = {
                                notSatisfiedDemands: []
                             },
            schedules = {
                            matched: [],
                            unmatched: []
                        };

        // Map cars to car groups, group the cars according to its features
        cars.map((item) => {
            // Initiate groups of car features
            let features = {
                model: item.model,
                engine: item.engine,
                infotainmentSystem: item.infotainmentSystem,
                interiorDesign: item.interiorDesign
            };
            let index = this.indexOfObjectInArray(carFeatureGroups.featureGroups, features);
            if (index === -1) {
                carFeatureGroups.featureGroups.push(features);
                carFeatureGroups.carGroups.push([item.carId]);
            } else {
                carFeatureGroups.carGroups[index].push(item.carId);
            }
            
        });

        // filter the unmatched demands according the desired car features
        demands.filter(item => {
            return timestamp < item.earliestPickUpTime;
        })
        .map(item => {
            let desired = item.desiredCarFeatures;
            let i = 0, length=carFeatureGroups.featureGroups.length;

            // To find a group which satisfies the desired features
            for (; i<length; i++) {

                if (this.isSubObject(desired, carFeatureGroups.featureGroups[i])) {
                    // Found matched group
                    let group = scheduleGroups[i.toString()];

                    if (group === undefined) {
                        group = [item];
                    } else {
                        group.push(item);
                    }
                    scheduleGroups[i.toString()] = group;
                    break;
                }
            }
            if (i === length) {
                scheduleGroups.notSatisfiedDemands.push(item);
            }
        });

        // Calcualte route
        Object.keys(scheduleGroups).slice(0, -1)
              .map((scheduleKey, index) => {
                let sortedArr = scheduleGroups[scheduleKey].sort((a, b) =>{
                    return a.earliestPickUpTime < b.earliestPickUpTime ? -1 : 
                            a.earliestPickUpTime == b.earliestPickUpTime ? 0 : 1;
                });

                let carsNow = carFeatureGroups.carGroups[parseInt(scheduleKey)].map(carId => {
                    let car = this.carCtr.getCarById(carId);
                    return {
                        carId: carId,
                        location: car.CurrentLocation
                    }
                }).sort((a, b) => {
                    return a.location < b.location ? 1 : 
                            a.location = b.location ? 0 : -1;
                })
                let results = this.getRoutes(sortedArr, carsNow);
                schedules.matched = schedules.matched.concat(results.matched);
                schedules.unmatched = schedules.unmatched.concat(results.unmatched);
              });
        schedules.unmatched = schedules.unmatched.concat(scheduleGroups.notSatisfiedDemands);
        return schedules;
    }

    /**
     * Get the most suitable route for a car
     * @param {array of sorted demands} demands 
     * @param {cars} cars 
     */
    getRoutes(demands, cars) {
        let schedules = {
                            matched: [],
                            unmatched: []
                        }, 
            restDemands = demands;

        // Find the closest demand for car
        for (let key in cars) {
            let car = cars[key],
                minDistance = 10000,
                demandIndex;

            // find the closest demand for this car
            restDemands.map((demand, ind) => {
                let distance = Math.abs(car.location - demand.pickUpLocation);
                if (distance < minDistance) {
                    minDistance = distance;
                    demandIndex = ind;
                }
            });
            
            if (demandIndex == null) {
                // No suitable demand found
                continue;
            } else {
                let subDemands = restDemands.slice(demandIndex),
                    sourceDemand = subDemands[0],
                    allRoutes = this.getAllRoutes(this.generateTree(restDemands), sourceDemand),
                    route = this.getShortestRoute(allRoutes),
                    schedule = {};

                schedule[car.carId] = route;

                // Insert route in schedules
                schedules.matched.push(schedule);

                // Filter the dispatched demands
                restDemands = restDemands.filter((demand) => {
                    let result = true;
                    route.map(routeDemand => {
                        if (routeDemand.demandId === demand.demandId) {
                            result = false;
                        }
                    });
                    return result;
                });
            }
        }

        // Insert unmatched demands
        if (restDemands.length > 0) {
            schedules.unmatched = restDemands;
        }

        return schedules;

    }

    /**
     * Calculate the distance of all routes and return the shortest one
     * @param {all possible routes} routes 
     */
    getShortestRoute(routes) {
        let maxValidDistance = 0, 
            minAllDistance = 10000, 
            targetRoute;

        // Iterate all routes and find the route with maximum valid distance
        // and minimum overall distance
        for(let route of routes) {
            let validDistance = 0, allDistance = 0;
            route.map((demand, ind) => {
                if (ind == 0) {
                    validDistance = Math.abs(demand.dropOffLocation - demand.pickUpLocation);
                    allDistance = Math.abs(demand.dropOffLocation - demand.pickUpLocation);
                } else {
                    let lastDemand = route[ind-1];
                    validDistance += Math.abs(demand.dropOffLocation - demand.pickUpLocation);
                    allDistance += Math.abs(demand.pickUpLocation - lastDemand.dropOffLocation) +
                                   Math.abs(lastDemand.dropOffLocation - lastDemand.pickUpLocation);
                }
            });
            
            if (validDistance >= maxValidDistance && allDistance <= minAllDistance) {
                targetRoute = route;
                maxValidDistance = validDistance;
                minAllDistance = allDistance;
            };
        }
        return targetRoute;
    }

    /**
     * All demands will be stored in a one-dimension array,
     * their possible child will be in an other one-dimension array,
     * they construct a 2 dimensional array, this is for returning 
     * all possible routes by searching by a demand id
     * 
     * @param {all demands} demands 
     */
    generateTree(demands) {

        let length = demands.length, 
            tree = {};

        // let restDemands = demands
        for (let i=0; i<length; i++) {
            let demand = demands[i]

            // Add parent node
            if (!(demand.demandId in tree)) {
                tree[demand.demandId] = [];
            }
            let subDemands = demands.slice(i+1);
            if (subDemands.length == 0) return tree;

            // Add its children
            for (let subDemand of subDemands) {
                if (subDemand.earliestPickUpTime > demand.latestDropOffTime) {
                    tree[demand.demandId].push(subDemand);
                }
            }
        }

        return tree;
    }
    /**
     * get all possible routes
     * @param {data structure generated by function generateTree} tree 
     * @param {demand as the root node} node 
     */
    getAllRoutes(tree, node) {
        let availableNodes = tree[node.demandId],
            allRoutes = [];

        // Empty tree
        if (availableNodes === undefined) return [[]];

        // No children, return current node as a route
        if (availableNodes.length == 0) return [[node]];

        for (let nextNode of availableNodes) {

            // Get all possible routes of this node
            let routes = this.getAllRoutes(tree, nextNode);

            if (routes.length == 1) {
                allRoutes.push([node].concat(routes[0]));
            } else {
                for(let route of routes) {
                    allRoutes.push([node].concat(route));
                }
            }
        }
        return allRoutes;
    }

    /**
     * Check if one object is contained by another
     * @param {object} child 
     * @param {object} parent 
     */
    isSubObject(child, parent) {

        if (typeof child !== "object" || typeof parent !== "object") return false;

        for (let key in child) {
            if (parent[key] === undefined) return false;

            if (child[key] !== parent[key]) return false;
        }
        
        return true;
    }

    /**
     * find the first index of an object in array
     * @param {objects array} arr 
     * @param {object} object 
     */
    indexOfObjectInArray(arr, object) {
        if (typeof object !== "object") return -2;

        if (!(arr instanceof Array)) return -2;

        for (let i=0, length=arr.length; i<length; i++) {
            if (typeof arr[i] === "object") {
                if (this.isObjectEqual(arr[i], object)) return i;
            }
        }

        return -1;
    }

    /**
     * Check if two objects are same
     * @param {object} a 
     * @param {object} b 
     */
    isObjectEqual(a, b) {
        if (Object.keys(a).length !== Object.keys(b).length) return false;

        for (let key in a) {
            if (!(key in b)) return false;

            if (a[key] !== b[key]) return false;
        }

        return true;
    }
}

module.exports = ScheduleService;