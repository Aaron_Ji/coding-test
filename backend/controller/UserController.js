const User = require("../model/user");
const { TargetNotFoundError, AssociationExistsError } = require("../errors/errors");

class UserController {
    constructor() {
        this.users = new Map();
    }

    static getInstance() {
        if (!this.userController) {
            this.userController = new UserController();
        }

        return this.userController;
    }

    // Only for test
    testAdd(user) {
        if (this.users.size == 0) {
            this.default = user;
        }
        user.userId = (this.users.size+1).toString();
        this.addUser(user);
    }

    /**
     * Get all users
     */
    getUsers() {
        return Array.from(this.users.values());
    }

    /**
     * Get user by user id
     * @param {user id} id 
     */
    getUserById(id) {
        let user = this.users.get(id);
        if (user === undefined) {
            throw new TargetNotFoundError("Get user failed", `Cannot find user by id ${id}`);
        }
        return user;
    }

    /**
     * Add new user
     * @param {instance of User} user 
     */
    addUser(user) {
        this.users.set(user.userId, user);
        return user;
    }

    /**
     * Update user features
     * @param {user id} id 
     * @param {user features: object} features 
     */
    updateUserById(id, features) {
        let user = this.getUserById(id);

        if (user === undefined) {
            throw new TargetNotFoundError("Update user failed", `Cannot find user by id ${id}`);
        }

        user.setFeatures(features);
        this.users.set(id, user);

        return user;
    }

    /**
     * Delete user by id
     * @param {user id} id 
     */
    deleteUserById(id) {
        if (!this.users.has(id)) {
            throw new TargetNotFoundError("Delete user failed", `Cannot find user by id ${id}`);
        }

        const DemandController = require("../controller/DemandController");
        if (DemandController.isUserContained(id)) {
            throw new AssociationExistsError("Delete user failed", `Cannot delete user while user has booked demands`);
        }

        this.users.delete(id);
    }

    /**
     * Check if user exists
     * @param {user id} id 
     */
    static isUserExisted(id) {
        return this.userController.users.has(id);
    }
}

module.exports = UserController;