const User = require("../model/user");
const Car = require("../model/car");
const Demand = require("../model/demand");

const UserController = require("../controller/UserController");
const DemandController = require("../controller/DemandController");
const CarController = require("../controller/CarController");

module.exports = ((userCtr, carCtr, demandCtr) => {

    // Add user data
    userCtr.testAdd(new User("Jhon", "male", 20));
    userCtr.testAdd(new User("Ada", "female", 18));
    userCtr.testAdd(new User("Crystal", "female", 24));

    // Add car data
    carCtr.testAdd(new Car("S", "V6", "Bose", "High level", 1));
    carCtr.testAdd(new Car("S", "V6", "Bose", "High level", 4));
    carCtr.testAdd(new Car("X", "V6", "Bose", "Low level", 8));
    carCtr.testAdd(new Car("3", "V6", "BOSCH", "Medium level", 4));
    carCtr.testAdd(new Car("X", "V8", "Bose", "High level", 0));
    carCtr.testAdd(new Car("X", "V8", "Bose", "High level", 7));
    carCtr.testAdd(new Car("X", "V8", "BMW", "Low level", 10));
    carCtr.testAdd(new Car("X", "V8", "BOSCH", "Medium level", 3));
    carCtr.testAdd(new Car("S", "V8", "BOSCH", "Low level", 22));
    carCtr.testAdd(new Car("3", "V8", "Bose", "Medium level", 50));
    

    // Add demand data
    demandCtr.testAdd(new Demand(userCtr.default.UserId, 0, 5, 100, 800, {
        model: "X",
        engine: "V8",
        interiorDesign: "High level"
    }));

    demandCtr.testAdd(new Demand("2", 0, 10, 1000, 60000, {
        model: "S",
        engine: "V6",
        infotainmentSystem: "Bose"
    }));

    demandCtr.testAdd(new Demand(userCtr.default.UserId, 6, 8, 1000, 6000, {
        model: "X",
        engine: "V6",
        interiorDesign: "High level"
    }));

    demandCtr.testAdd(new Demand("2", 15, 40, 100000, 102000, {
        model: "S",
        engine: "V6"
    }));

    demandCtr.testAdd(new Demand(userCtr.default.UserId, 8, 20, 2000, 5000, {
        model: "X",
        engine: "V8",
        interiorDesign: "High level"
    }));

    demandCtr.testAdd(new Demand(userCtr.default.UserId, 6, 18, 2000, 5000, {
        model: "X",
        engine: "V8",
        interiorDesign: "High level"
    }));
    

})(UserController.getInstance(), CarController.getInstance(), DemandController.getInstance())