const {expect} = require("chai");
const supertest = require("supertest");

const api = supertest("http://localhost:8888/api");
const demandId = "1";

describe("Demand Test", () => {

    /**
     * Test API: GET /api/demands
     * Cases:
     *      1. Code 200: GET /api/demands => return demands array
     *      2. Code 404: GET /api/demand   => invalid api
     */
    it("Should return an array of demands", (done) => {
        api.get("/demands")
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data).to.be.a("array");
                expect(res.body.data[0]).to.have.property("demandId");
                expect(res.body.data[0]).to.have.property("userId");
                expect(res.body.data[0]).to.have.property("pickUpLocation");
                expect(res.body.data[0]).to.have.property("dropOffLocation");
                expect(res.body.data[0]).to.have.property("earliestPickUpTime");
                expect(res.body.data[0]).to.have.property("latestDropOffTime");
                expect(res.body.data[0]).to.have.property("desiredCarFeatures");
                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.get("/demand")
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404)
                done();
           });
    });

    /**
     * Test API: GET /api/demands/:id
     * Cases:
     *      1. Code 200: GET /api/demands/1 => return json of demand
     *      2. Code 404: GET /api/demands/101 => not found demand
     *      3. Code 404: GET /api/demand/1  => invalid api
     */
    it("Should return a specific demand", (done) => {
        api.get(`/demands/${demandId}`)
           .expect(200)
           .end((err, res) => {
                if(err) {
                    done(err);
                }
                expect(res.body.data).to.have.property("demandId");
                expect(res.body.data).to.have.property("userId");
                expect(res.body.data).to.have.property("pickUpLocation");
                expect(res.body.data).to.have.property("dropOffLocation");
                expect(res.body.data).to.have.property("earliestPickUpTime");
                expect(res.body.data).to.have.property("latestDropOffTime");
                expect(res.body.data).to.have.property("desiredCarFeatures");
                expect(res.body.data.demandId).to.equal("1");
                done();
           });
    });

    it("Should return error with 404 when demand cannot be found by id", (done) => {
        api.get(`/demands/${demandId+100}`)
           .expect(200)
           .end((err, res) => {
               if(err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.get(`/demand/${demandId}`)
           .expect(200)
           .end((err, res) => {
               if(err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });
    
    /**
     * Test API: POST /api/demands with value in body
     * Cases:
     *      1. Code 200: POST /api/demands => return a created demand
     *         body: { 
     *                  "userId": "1",
     *                  "pickUpLocation": 10, 
     *                  "dropOffLocation": 300, 
     *                  "earliestPickUpTime": 500,
     *                  "latestDropOffTime": 1000,
     *                  "desiredCarFeatures": {
     *                      "model": "S",
     *                      "interiorDesign": "high level",
     *                  }
     *               }
     *      2. Code 404: POST /api/demand  => invalid api
     *      3. Code 404: POST /api/demands => create a demand with an not existed user (user id: "ABC")
     */
    it("Should return a created demand", (done) => {
        api.post("/demands")
           .send({ 
                "userId": "1",
                "pickUpLocation": 10, 
                "dropOffLocation": 300, 
                "earliestPickUpTime": 500,
                "latestDropOffTime": 1000,
                "desiredCarFeatures": {
                    "model": "S",
                    "interiorDesign": "high level",
                }
            })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data).to.have.property("demandId");
                expect(res.body.data).to.have.property("userId");
                expect(res.body.data).to.have.property("pickUpLocation");
                expect(res.body.data).to.have.property("dropOffLocation");
                expect(res.body.data).to.have.property("earliestPickUpTime");
                expect(res.body.data).to.have.property("latestDropOffTime");
                expect(res.body.data).to.have.property("desiredCarFeatures");
                expect(res.body.data.userId).to.equal("1");
                expect(res.body.data.pickUpLocation).to.equal(10);
                expect(res.body.data.dropOffLocation).to.equal(300);
                expect(res.body.data.earliestPickUpTime).to.equal(500);
                expect(res.body.data.latestDropOffTime).to.equal(1000);
                expect(res.body.data.desiredCarFeatures).to.have.property("model");
                expect(res.body.data.desiredCarFeatures).to.have.property("interiorDesign");
                expect(res.body.data.desiredCarFeatures.model).to.equal("S");
                expect(res.body.data.desiredCarFeatures.interiorDesign).to.equal("high level");
                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.post("/demand")
           .send({
                "userId": "1",
                "pickUpLocation": 10, 
                "dropOffLocation": 300, 
                "earliestPickUpTime": 500,
                "latestDropOffTime": 1000,
                "desiredCarFeatures": {
                    "model": "S",
                    "interiorDesign": "high level",
                }
           })
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    it("Should return error with 404 when user does not exists", (done) => {
        api.post("/demands")
           .send({
                "userId": "ABC",
                "pickUpLocation": 10, 
                "dropOffLocation": 300, 
                "earliestPickUpTime": 500,
                "latestDropOffTime": 1000,
                "desiredCarFeatures": {
                    "model": "S",
                    "interiorDesign": "high level",
                }
           })
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    /**
     * Test API: PUT /api/demands/:id/:feature with value in body
     * Cases:
     *      1. Code 200: PUT /api/demands/1/pickUpLocation => return a changed demand
     *         body: { "value": 88 }
     *      2. Code 404: PUT /api/demand/1/pickUpLocation  => invalid api
     *         body: { "value": 88 }
     *      3. Code 404: PUT /api/demands/101/pickUpLocation => demand not found
     *         body: { "value": 88 }
     *      4. Code 404: PUT /api/demands/1/nationality => feature not found
     *         body: { "value": "the United States" }
     */
    it("Should return a changed demand", (done) => {
        api.put(`/demands/${demandId}/pickUpLocation`)
           .send({
               'value': 88
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data.pickUpLocation).to.equal(88);
                api.get(`/demands/${demandId}`)
                    .expect(200)
                    .end((err, res) => {
                            if (err) {
                                done(err);
                            }
                            expect(res.body.data).to.have.property("demandId");
                            expect(res.body.data).to.have.property("userId");
                            expect(res.body.data).to.have.property("pickUpLocation");
                            expect(res.body.data).to.have.property("dropOffLocation");
                            expect(res.body.data).to.have.property("earliestPickUpTime");
                            expect(res.body.data).to.have.property("latestDropOffTime");
                            expect(res.body.data).to.have.property("desiredCarFeatures");
                            expect(res.body.data.pickUpLocation).to.equal(88);

                    });
                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.put(`/demand/${demandId}/pickUpLocation`)
           .send({
               'value': 88
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404);
                done();
           });
    });

    it("Should return error with 404 when demand is not found", (done) => {
        api.put(`/demands/${demandId+100}/pickUpLocation`)
           .send({
               'value': 88
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404);
                done();
           });
    });

    it("Should return error with 404 when demand feature is not found", (done) => {
        api.put(`/demands/${demandId}/nationality`)
           .send({
               'value': 'US'
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404);
                done();
           });
    });

    /**
     * Test API PUT /api/demands/:id with value in body
     * Cases:
     *      1. Code 200: PUT /api/demands/1 => return changed demand
     *         body: { "dropOffLocation": 10, "earliestPickUpTime": 50000 }
     *      2. Code 404: PUT /api/demand/1  => invalid api
     *         body: { "dropOffLocation": 10, "earliestPickUpTime": 50000 }
     *      3. Code 404: PUT /api/demands/101 => demand not found
     *         body: { "dropOffLocation": 10, "earliestPickUpTime": 50000 }
     *      4. Code 404: PUT /api/demands/1  => features invalid
     *         body: { "dropOffLocation": 10, "earliestPickUpTime": 50000, "nationality": "US" }
     */
    it("Should return changed demand", (done) => {
        api.put(`/demands/${demandId}`)
           .send({
               dropOffLocation: 10,
               earliestPickUpTime: 50000
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                
                expect(res.body.data).to.have.property("demandId");
                expect(res.body.data).to.have.property("userId");
                expect(res.body.data).to.have.property("pickUpLocation");
                expect(res.body.data).to.have.property("dropOffLocation");
                expect(res.body.data).to.have.property("earliestPickUpTime");
                expect(res.body.data).to.have.property("latestDropOffTime");
                expect(res.body.data).to.have.property("desiredCarFeatures");
                expect(res.body.data.dropOffLocation).to.equal(10);
                expect(res.body.data.earliestPickUpTime).to.equal(50000);

                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.put(`/demand/${demandId}`)
           .send({
               dropOffLocation: 10,
               earliestPickUpTime: 50000
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.status).to.equal(404);

                done();
           });
    });

    it("Should return error with 404 when demand is not found", (done) => {
        api.put(`/demands/${demandId+100}`)
           .send({
               dropOffLocation: 10,
               earliestPickUpTime: 50000
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.status).to.equal(404);

                done();
           });
    });

    it("Should return error with 404 when feature is belong to demand", (done) => {
        api.put(`/demands/${demandId}`)
           .send({
               dropOffLocation: 10,
               earliestPickUpTime: 50000,
               nationality: "US"
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.status).to.equal(404);

                done();
           });
    });

    /**
     * 
     * Test API DELETE /api/demands/:id
     * Cases:
     *      1. Code 200: DELETE /api/demands/1
     *      2. Code 404: DELETE /api/demand/1    => invalid api
     *      3. Code 404: DELETE /api/demands/101 => demand not found
     */
    it("Should successfully delete the demand by id", (done) => {
        api.delete(`/demands/1`)
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(200);
               done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.delete(`/demand/${demandId}`)
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    it("Should return error with 404 when demand is not found", (done) => {
        api.delete(`/demands/${demandId+100}`)
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });
});