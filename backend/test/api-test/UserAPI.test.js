const {expect} = require("chai");
const supertest = require("supertest");

const api = supertest("http://localhost:8888/api");
const userId = "1";

describe("User", () => {

    /**
     * Test API: GET /api/users
     * Cases:
     *      1. Code 200: GET /api/users => return users array
     *      2. Code 404: GET /api/usr   => invalid api
     */
    it("Should return an array of users", (done) => {
        api.get("/users")
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data).to.be.a("array");
                expect(res.body.data[0]).to.have.property("userId");
                expect(res.body.data[0]).to.have.property("name");
                expect(res.body.data[0]).to.have.property("gender");
                expect(res.body.data[0]).to.have.property("age");
                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.get("/usr")
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404)
                done();
           });
    });

    /**
     * Test API: GET /api/users/:id
     * Cases:
     *      1. Code 200: GET /api/users/1 => return json of user
     *      2. Code 404: GET /api/users/101 => not found user
     *      3. Code 404: GET /api/user/1  => invalid api
     */
    it("Should return a specific user", (done) => {
        api.get(`/users/${userId}`)
           .expect(200)
           .end((err, res) => {
                if(err) {
                    done(err);
                }
                expect(res.body.data).to.have.property("userId");
                expect(res.body.data).to.have.property("name");
                expect(res.body.data).to.have.property("gender");
                expect(res.body.data).to.have.property("age");
                expect(res.body.data.userId).to.equal("1");
                done();
           });
    });

    it("Should return error with 404 when user cannot be found by id", (done) => {
        api.get(`/users/${userId+100}`)
           .expect(200)
           .end((err, res) => {
               if(err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.get(`/user/${userId}`)
           .expect(200)
           .end((err, res) => {
               if(err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });
    
    /**
     * Test API: POST /api/users with value in body
     * Cases:
     *      1. Code 200: POST /api/users => return a created user
     *         body: { "name": "Jim", "gender": "male", "age": 33}
     *      2. Code 404: POST /api/user  => invalid api
     *         body: { "name": "Jim", "gender": "male", "age": 33}
     */
    it("Should return a created user", (done) => {
        api.post("/users")
           .send({
               'name': 'Jim',
               'gender': 'male',
               'age': 33
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data).to.have.property("userId");
                expect(res.body.data).to.have.property("name");
                expect(res.body.data).to.have.property("gender");
                expect(res.body.data).to.have.property("age");
                expect(res.body.data.name).to.equal("Jim");
                expect(res.body.data.gender).to.equal("male");
                expect(res.body.data.age).to.equal(33);

                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.post("/user")
           .send({
               'name': 'Jim',
               'gender': 'male',
               'age': 33
           })
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    /**
     * Test API: PUT /api/users/:id/:feature with value in body
     * Cases:
     *      1. Code 200: PUT /api/users/1/name => return a changed user
     *         body: { "value": "Johny" }
     *      2. Code 404: PUT /api/user/1/name  => invalid api
     *         body: { "value": "Johny" }
     *      3. Code 404: PUT /api/users/101/name => user not found
     *         body: { "value": "Johny" }
     *      4. Code 404: PUT /api/users/1/nationality => feature not found
     *         body: { "value": "the United States" }
     */
    it("Should return a changed user", (done) => {
        api.put(`/users/${userId}/name`)
           .send({
               'value': 'Johny'
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data.name).to.equal("Johny");
                api.get(`/users/${userId}`)
                    .expect(200)
                    .end((err, res) => {
                            if (err) {
                                done(err);
                            }
                            expect(res.body.data).to.have.property("userId");
                            expect(res.body.data).to.have.property("name");
                            expect(res.body.data).to.have.property("gender");
                            expect(res.body.data).to.have.property("age");
                            expect(res.body.data.name).to.equal("Johny");

                    });
                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.put(`/user/${userId}/name`)
           .send({
               'name': 'Johny'
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404);
                done();
           });
    });

    it("Should return error with 404 when user is not found", (done) => {
        api.put(`/users/${userId+100}/name`)
           .send({
               'name': 'Johny'
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404);
                done();
           });
    });

    it("Should return error with 404 when user feature is not found", (done) => {
        api.put(`/users/${userId}/nationality`)
           .send({
               'value': 'US'
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                
                expect(res.body.status).to.equal(404);
                done();
           });
    });

    /**
     * Test API PUT /api/users/:id with value in body
     * Cases:
     *      1. Code 200: PUT /api/users/1 => return changed user
     *         body: { "gender": "Female", "age": 18 }
     *      2. Code 404: PUT /api/user/1  => invalid api
     *         body: { "gender": "Female", "age": 18 }
     *      3. Code 404: PUT /api/users/101 => user not found
     *         body: { "gender": "Female", "age": 18 }
     *      4. Code 404: PUT /api/users/1  => features invalid
     *         body: { "gender": "Female", "age": 18, "nationality": "US" }
     */
    it("Should return changed user", (done) => {
        api.put(`/users/${userId}`)
           .send({
               gender: "Female",
               age: 18
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data).to.have.property("userId");
                expect(res.body.data).to.have.property("name");
                expect(res.body.data).to.have.property("gender");
                expect(res.body.data).to.have.property("age");
                expect(res.body.data.gender).to.equal("Female");
                expect(res.body.data.age).to.equal(18);

                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.put(`/user/${userId}`)
           .send({
               gender: "Female",
               age: 18
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.status).to.equal(404);

                done();
           });
    });

    it("Should return error with 404 when user is not found", (done) => {
        api.put(`/users/${userId+100}`)
           .send({
               gender: "Female",
               age: 18
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.status).to.equal(404);

                done();
           });
    });

    it("Should return error with 404 when feature is belong to user", (done) => {
        api.put(`/users/${userId}`)
           .send({
               gender: "Female",
               age: 18,
               nationality: "US"
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.status).to.equal(404);

                done();
           });
    });

    /**
     * 
     * Test API DELETE /api/users/:id
     * Cases:
     *      1. Code 200: DELETE /api/users/2
     *      2. Code 404: DELETE /api/user/1    => invalid api
     *      3. Code 404: DELETE /api/users/101 => user not found
     *      4. Code 404: DELETE /api/users/1   => user can not be deleted when user has booked demand
     */
    it("Should successfully delete the user by id", (done) => {
        api.delete(`/users/3`)
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(200);
               done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.delete(`/user/${userId}`)
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    it("Should return error with 404 when user is not found", (done) => {
        api.delete(`/users/${userId+100}`)
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    it("Should return error with 404 when user has booked demands", (done) => {
        api.delete(`/users/${userId}`)
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });
})