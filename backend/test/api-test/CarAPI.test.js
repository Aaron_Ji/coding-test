const {expect} = require("chai");
const supertest = require("supertest");

const api = supertest("http://localhost:8888/api");
const carId = "1";

describe("Car API Test", () => {

    /**
     * Test API: GET /api/cars
     * Cases:
     *      1. Code 200: GET /api/cars => return cars array
     *      2. Code 404: GET /api/car   => invalid api
     */
    it("Should return an array of cars", (done) => {
        api.get("/cars")
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data).to.be.a("array");
                expect(res.body.data[0]).to.have.property("carId");
                expect(res.body.data[0]).to.have.property("model");
                expect(res.body.data[0]).to.have.property("engine");
                expect(res.body.data[0]).to.have.property("infotainmentSystem");
                expect(res.body.data[0]).to.have.property("interiorDesign");
                expect(res.body.data[0]).to.have.property("currentLocation");
                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.get("/car")
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.error).to.equal("Cannot GET /api/car");
                expect(res.body.reason).to.equal("Invalid URL request");
                expect(res.body.status).to.equal(404)
                done();
           });
    });

    /**
     * Test API: GET /api/cars/:id
     * Cases:
     *      1. Code 200: GET /api/cars/1 => return json of car
     *      2. Code 404: GET /api/cars/101 => not found car
     *      3. Code 404: GET /api/car/1  => invalid api
     */
    it("Should return a specific car", (done) => {
        api.get(`/cars/${carId}`)
           .expect(200)
           .end((err, res) => {
                if(err) {
                    done(err);
                }
                expect(res.body.data).to.have.property("carId");
                expect(res.body.data).to.have.property("model");
                expect(res.body.data).to.have.property("engine");
                expect(res.body.data).to.have.property("infotainmentSystem");
                expect(res.body.data).to.have.property("interiorDesign");
                expect(res.body.data).to.have.property("currentLocation");
                expect(res.body.data.carId).to.equal("1");
                done();
           });
    });

    it("Should return error with 404 when car cannot be found by id", (done) => {
        api.get(`/cars/${carId+100}`)
           .expect(200)
           .end((err, res) => {
               if(err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.get(`/car/${carId}`)
           .expect(200)
           .end((err, res) => {
               if(err) {
                   done(err);
               }
               expect(res.body.error).to.equal("Cannot GET /api/car/1");
               expect(res.body.status).to.equal(404);
               done();
           });
    });
    
    /**
     * Test API: POST /api/cars with value in body
     * Cases:
     *      1. Code 200: POST /api/cars => return a created car
     *         body: { "model": "X", "engine": "V8", "infotainmentSystem": "BOSH"}
     *      2. Code 404: POST /api/car  => invalid api
     *         body: { "model": "X", "engine": "V8", "infotainmentSystem": "BOSH"}
     */
    it("Should return a created car", (done) => {
        api.post("/cars")
           .send({
               'model': 'X',
               'engine': 'V8',
               'infotainmentSystem': "BOSH"
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data).to.have.property("carId");
                expect(res.body.data).to.have.property("model");
                expect(res.body.data).to.have.property("engine");
                expect(res.body.data).to.have.property("infotainmentSystem");
                expect(res.body.data).to.have.property("interiorDesign");
                expect(res.body.data).to.have.property("currentLocation");
                expect(res.body.data.model).to.equal("X");
                expect(res.body.data.engine).to.equal("V8");
                expect(res.body.data.infotainmentSystem).to.equal("BOSH");

                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.post("/car")
           .send({
               'model': 'X',
               'engine': 'V8',
               'infotainmentSystem': "BOSH"
           })
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.error).to.equal("Cannot POST /api/car");
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    /**
     * Test API: PUT /api/cars/:id/:feature with value in body
     * Cases:
     *      1. Code 200: PUT /api/cars/1/model => return a changed car
     *         body: { "value": "S" }
     *      2. Code 404: PUT /api/car/1/model  => invalid api
     *         body: { "value": "S" }
     *      3. Code 404: PUT /api/cars/101/model => car not found
     *         body: { "value": "S" }
     *      4. Code 404: PUT /api/cars/1/producer => feature not found
     *         body: { "value": "the United States" }
     */
    it("Should return a changed car", (done) => {
        api.put(`/cars/${carId}/model`)
           .send({
               'value': 'S'
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data.model).to.equal("S");
                api.get(`/cars/${carId}`)
                    .expect(200)
                    .end((err, res) => {
                            if (err) {
                                done(err);
                            }
                            expect(res.body.data).to.have.property("carId");
                            expect(res.body.data).to.have.property("model");
                            expect(res.body.data).to.have.property("engine");
                            expect(res.body.data).to.have.property("infotainmentSystem");
                            expect(res.body.data).to.have.property("interiorDesign");
                            expect(res.body.data).to.have.property("currentLocation");
                            expect(res.body.data.model).to.equal("S");

                    });
                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.put(`/car/${carId}/model`)
           .send({
               'model': 'S'
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.error).to.equal("Cannot PUT /api/car/1/model");
                expect(res.body.status).to.equal(404);
                done();
           });
    });

    it("Should return error with 404 when car is not found", (done) => {
        api.put(`/cars/${carId+100}/model`)
           .send({
               'model': 'S'
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404);
                done();
           });
    });

    it("Should return error with 404 when car feature is not found", (done) => {
        api.put(`/cars/${carId}/producer`)
           .send({
               'value': 'US'
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                // console.log(res.body)
                expect(res.body.status).to.equal(404);
                done();
           });
    });

    /**
     * Test API PUT /api/cars/:id with value in body
     * Cases:
     *      1. Code 200: PUT /api/cars/1 => return changed car
     *         body: { "engine": "V8", "infotainmentSystem": "BMW" }
     *      2. Code 404: PUT /api/car/1  => invalid api
     *         body: { "engine": "V8", "infotainmentSystem": "BMW" }
     *      3. Code 404: PUT /api/cars/101 => car not found
     *         body: { "engine": "V8", "infotainmentSystem": "BMW" }
     *      4. Code 404: PUT /api/cars/1  => features invalid
     *         body: { "engine": "V8", "infotainmentSystem": "BMW", "producer": "US" }
     */
    it("Should return changed car", (done) => {
        api.put(`/cars/${carId}`)
           .send({
               engine: "V8",
               infotainmentSystem: "BMW"
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data).to.have.property("carId");
                expect(res.body.data).to.have.property("model");
                expect(res.body.data).to.have.property("engine");
                expect(res.body.data).to.have.property("infotainmentSystem");
                expect(res.body.data).to.have.property("interiorDesign");
                expect(res.body.data).to.have.property("currentLocation");
                expect(res.body.data.engine).to.equal("V8");
                expect(res.body.data.infotainmentSystem).to.equal("BMW");

                done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.put(`/car/${carId}`)
           .send({
               engine: "V8",
               infotainmentSystem: "BMW"
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.status).to.equal(404);

                done();
           });
    });

    it("Should return error with 404 when car is not found", (done) => {
        api.put(`/cars/${carId+100}`)
           .send({
               engine: "V8",
               infotainmentSystem: "BMW"
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.status).to.equal(404);

                done();
           });
    });

    it("Should return error with 404 when feature is belong to car", (done) => {
        api.put(`/cars/${carId}`)
           .send({
               engine: "V8",
               infotainmentSystem: "BMW",
               producer: "US"
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }

                expect(res.body.status).to.equal(404);

                done();
           });
    });

    /**
     * 
     * Test API DELETE /api/cars/:id
     * Cases:
     *      1. Code 200: DELETE /api/cars/1
     *      2. Code 404: DELETE /api/car/1    => invalid api
     *      3. Code 404: DELETE /api/cars/101 => car not found
     */
    it("Should successfully delete the car by id", (done) => {
        api.delete(`/cars/1`)
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(200);
               done();
           });
    });

    it("Should return error with 404 when api is invalid", (done) => {
        api.delete(`/car/${carId}`)
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });

    it("Should return error with 404 when car is not found", (done) => {
        api.delete(`/cars/${carId+100}`)
           .expect(200)
           .end((err, res) => {
               if (err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               done();
           });
    });
});