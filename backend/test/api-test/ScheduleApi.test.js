const {expect} = require("chai");
const supertest = require("supertest");

const api = supertest("http://localhost:8888/api");

describe("Schedule API Test", () => {

    /**
     * Test API: GET /api/schedules
     * Cases:
     *      1. Code 200: GET /api/schedules => return schedules (an object)
     *      2. Code 404: GET /api/schedule   => invalid api
     */
    it("Should return schedules", (done) => {
        api.get("/schedules")
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.data).to.be.a("object");
                expect(res.body.data).to.have.property("matched");
                expect(res.body.data).to.have.property("unmatched");
                expect(res.body.data.matched.length > 0).to.equal(true);
                expect(res.body.data.unmatched.length > 0).to.equal(true);
                done();
           });
    });

    it("Should return Cannot GET /api/schedule", (done) => {
        api.get("/schedule")
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.error).to.equal("Cannot GET /api/schedule");
                expect(res.body.reason).to.equal("Invalid URL request");
                expect(res.body.status).to.equal(404)
                done();
           });
    });

    /**
     * Test API: GET /api/schedules/:id
     * Cases:
     *      1. Code 404: GET /api/schedules/1 => invalid api
     */

    it("Should return Cannot GET /api/schedules/1", (done) => {
        api.get(`/schedules/1`)
           .expect(200)
           .end((err, res) => {
               if(err) {
                   done(err);
               }
               expect(res.body.status).to.equal(404);
               expect(res.body.error).to.equal("Cannot GET /api/schedules/1");
               expect(res.body.reason).to.equal("Invalid URL request");
               
               done();
           });
    });
    
    /**
     * Test API: POST /api/schedules with value in body
     * Cases:
     *      1. Code 404: POST /api/schedules  => invalid api
     *         body: { "model": "X", "engine": "V8", "infotainmentSystem": "BOSH"}
     */
    it("Should return Cannot POST /api/schedules", (done) => {
        api.post("/schedules")
           .send({
               'model': 'X',
               'engine': 'V8',
               'infotainmentSystem': "BOSH"
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404);
                expect(res.body.error).to.equal("Cannot POST /api/schedules");
                expect(res.body.reason).to.equal("Invalid URL request");
                done();
           });
    });

    /**
     * Test API: PUT /api/schedules/:id/:feature
     * Cases:
     *      1. Code 404: PUT /api/schedules/1/model  => invalid api
     *         body: { "value": "S" }
     */
    it("Should return Cannot POST /api/schedules/1/model", (done) => {
        api.put(`/schedules/1/model`)
           .send({
               'value': 'S'
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404);
                expect(res.body.error).to.equal("Cannot PUT /api/schedules/1/model");
                expect(res.body.reason).to.equal("Invalid URL request");
                done();
           });
    });

    /**
     * Test API PUT /api/schedules/:id with value in body
     * Cases:
     *      1. Code 404: PUT /api/car/1  => invalid api
     *         body: { "engine": "V8", "infotainmentSystem": "BMW" }
     */
    it("Should return Cannot PUT /api/schedules/1", (done) => {
        api.put(`/schedules/1`)
           .send({
               engine: "V8",
               infotainmentSystem: "BMW"
           })
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404);
                expect(res.body.error).to.equal("Cannot PUT /api/schedules/1");
                expect(res.body.reason).to.equal("Invalid URL request");
                done();
           });
    });

    /**
     * 
     * Test API DELETE /api/schedules/:id
     * Cases:
     *      1. Code 404: DELETE /api/schedules/1 => invalid api
     */
    it("Should return Cannot DELETE /api/schedules/1", (done) => {
        api.delete(`/schedules/1`)
           .expect(200)
           .end((err, res) => {
                if (err) {
                    done(err);
                }
                expect(res.body.status).to.equal(404);
                expect(res.body.error).to.equal("Cannot DELETE /api/schedules/1");
                expect(res.body.reason).to.equal("Invalid URL request");
                done();
           });
    });
});