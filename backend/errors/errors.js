/**
 * Make Error extensible
 */
class ExtendableError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
        if (typeof Error.captureStackTrace === 'function') {
            Error.captureStackTrace(this, this.constructor);
        } else { 
            this.stack = (new Error(message)).stack; 
        }
    }
}   

/**
 * URL Error
 */
class URLError extends ExtendableError {
    constructor(message, reason) {
        super(message);
        this.name = "URLError";
        this.reason = reason;
    }
}

/**
 * Handle target not found error: user not found
 */
class TargetNotFoundError extends ExtendableError {
    constructor(message, reason) {
        super(message);
        this.name = "TargetNotFoundError";
        this.reason = reason;
    }
}

/**
 * Handle association still existing error: user cannot be deleted
 * when he has a demand
 */
class AssociationExistsError extends ExtendableError {
    constructor(message, reason) {
        super(message);
        this.name = "AssociationExistsError";
        this.reason = reason;
    }
}

module.exports = {
    URLError: URLError,
    TargetNotFoundError: TargetNotFoundError,
    AssociationExistsError: AssociationExistsError
}