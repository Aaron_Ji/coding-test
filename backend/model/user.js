let utils = require("../controller/utils");
const { URLError, TargetNotFoundError, AssociationExistsError } = require("../errors/errors");

class User {
    // User attributes
    constructor(name, gender, age) {
        // this.attr = {
        //     userId: utils.uuid(8, 16),
        //     name: name || "",
        //     gender: gender || "",
        //     age: age || 0,
        // }
        
        this.userId = utils.uuid(8, 16);
        this.name = name || "";
        this.gender = gender || "male";
        this.age = age || 0;
    }

    // Get user id
    get UserId() {
        return this.userId;
    }
    // Set user name
    set Name(name) {
        this.name = name;
    }
    // Get user name
    get Name() {
       return this.name;
    }
    // Set user gender
    set Gender(gender) {
        this.gender = gender;
    }
    // Get user gender
    get Gender() {
        return this.gender;
    }
    // Set user age
    set Age(age) {
        this.age = age;
    }
    // Get user gage
    get Age() {
        return this.age;
    }

    /**
     * Set user feature
     * @param {object key} feature 
     * @param {object value} value 
     */
    setFeature(feature, value) {
        
        if(feature === "userId") {
            throw new TargetNotFoundError("Update user failed", "Can not set user id");
        } else if ((feature in this) === false ) {
            throw new TargetNotFoundError("Update user failed", "No such feature");
        } else {
            this[utils.captialize(feature)] = value;
        }
    }

    /**
     * Update feautures by object
     * @param {object} features 
     */
    setFeatures(features) {
        Object.keys(features).map((feature) => {
            this.setFeature(feature, features[feature]);
        });
    }
}

module.exports = User;