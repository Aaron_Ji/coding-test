const utils = require("../controller/utils");
const { TargetNotFoundError } = require("../errors/errors");

class Demand {
    // Demand attributes
    constructor(userId, pickUpLocation, dropOffLocation, earliestPickUpTime, latestDropOffTime, desiredCarFeatures) {
        // this.attr = {
        //     demandId: utils.uuid(8, 16),
        //     userId: userId,
        //     pickUpLocation: pickUpLocation,
        //     dropOffLocation: dropOffLocation,
        //     earliestPickUpTime: earliestPickUpTime,
        //     latestDropOffTime: latestDropOffTime,
        //     desiredCarFeatures: desiredCarFeatures || {},
        // }
        this.demandId = utils.uuid(8, 16);
        this.userId = userId;
        this.pickUpLocation = pickUpLocation;
        this.dropOffLocation = dropOffLocation;
        this.earliestPickUpTime = earliestPickUpTime;
        this.latestDropOffTime = latestDropOffTime;
        this.desiredCarFeatures = desiredCarFeatures || {};
    }

    // Get user ID
    get UserId() {
        return this.userId;
    }
    // Get demand id
    get DemandId() {
        return this.demandId;
    }
    // Set pick-up location
    set PickUpLocation(location) {
        this.pickUpLocation = location;
    }
    // Get pick-up location
    get PickUpLocation() {
       return this.pickUpLocation;
    }
    // Set drop-off location
    set DropOffLocation(location) {
        this.dropOffLocation = location;
    }
    // Get drop-off location
    get DropOffLocation() {
        return this.dropOffLocation;
    }
    // Set earlist pick-up time
    set EarliestPickUpTime(time) {
        this.earliestPickUpTime = time;
    }
    // Get earlist pick-up time
    get EarliestPickUpTime() {
        return this.earliestPickUpTime;
    }
    // Set latest drop-off time
    set LatestDropOffTime(time) {
        this.latestDropOffTime = time;
    }
    // Get latest drop-off time
    get LatestDropOffTime() {
        return this.latestDropOffTime;
    }
    // Set desired car features
    set DesiredCarFeatures(features) {
        // Object.keys(features).map((key) => {
        //     this.attr.desiredCarFeatures[key] = features[key];
        // })
        this.desiredCarFeatures = features;
    }
    // Get desired car features
    get DesiredCarFeatures() {
        return this.desiredCarFeatures;
    }

    /**
     * Set demand feature
     * @param {object key} feature 
     * @param {object value} value 
     */
    setFeature(feature, value) {
        if(feature === "demandId") {
            throw new TargetNotFoundError("Update demand failed", "Can not set demand id");
        }
        if((feature in this) === false ) {
            throw new TargetNotFoundError("Update demand failed", "No such feature");
        }  
        
        this[utils.captialize(feature)] = value;
    }
    
    /**
     * Update feautures by object
     * @param {object} features 
     */
    setFeatures(features) {
        Object.keys(features).map((feature) => {
            this.setFeature(feature, features[feature]);
        });
    }

}

module.exports = Demand;