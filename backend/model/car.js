let utils = require("../controller/utils");
const {TargetNotFoundError} = require("../errors/errors");

class Car {
    // Car attributes
    constructor(model, engine, infotainmentSystem, interiorDesign, currentLocation) {
        // this.attr = {
        //     carId: utils.uuid(8, 16),
        //     model: model || "",
        //     engine: engine || "",
        //     infotainmentSystem: infotainmentSystem || "",
        //     interiorDesign: interiorDesign || "",
        //     currentLocation: currentLocation || 0
        // }

        this.carId = utils.uuid(8, 16);
        this.model =  model || "";
        this.engine = engine || "";
        this.infotainmentSystem = infotainmentSystem || "";
        this.interiorDesign = interiorDesign || "";
        this.currentLocation = currentLocation || 0;
}

    // Get car ID
    get CarId() {
        return this.carId;
    }
    // Set car model
    set Model(model) {
        this.model = model;
    }
    // Get car model
    get Model() {
       return this.model;
    }
    // Set car engine
    set Engine(engine) {
        this.engine = engine;
    }
    // Get car engine
    get Engine() {
        return this.engine;
    }
    // Set car infotainment system
    set InfotainmentSystem(infotainmentSystem) {
        this.infotainmentSystem = infotainmentSystem;
    }
    // Get car infotainment system
    get InfotainmentSystem() {
        return this.infotainmentSystem;
    }
    // Set car interior design
    set InteriorDesign(interiorDesign) {
        this.interiorDesign = interiorDesign;
    }
    // Get car interior design
    get InteriorDesign() {
        return this.interiorDesign;
    }
    // Set car current location
    set CurrentLocation(currentLocation) {
        this.currentLocation = currentLocation;
    }
    // Get car current location
    get CurrentLocation() {
        return this.currentLocation;
    }
    /**
     * Update car feature
     * @param {object key} feature 
     * @param {object value} value 
     */
    setFeature(feature, value) {
        if(feature === "carId") {
            throw new TargetNotFoundError("Update car failed", "Can not set car id");
        } else if ((feature in this) === false ) {
            throw new TargetNotFoundError("Update car failed", "No such feature");
        } else {
            this[utils.captialize(feature)] = value;
        }
    }

    /**
     * Update car features by object
     * @param {object} features 
     */
    setFeatures(features) {
        Object.keys(features).map((feature) => {
            this.setFeature(feature, features[feature]);
        });
    }

}

module.exports = Car;