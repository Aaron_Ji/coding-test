const app = require("./controller/ServerController").getInstance();
const fs = require('fs');

const { URLError } = require("./errors/errors");

const userController = require("./controller/UserController").getInstance();
const demandController = require("./controller/DemandController").getInstance();
const carController = require("./controller/CarController").getInstance();
const scheduleService = require("./controller/ScheduleService").getInstance();
const User = require("./model/user");
const Car = require("./model/car");
const Demand = require("./model/demand");
const utils = require("./controller/utils");

require("./test/add.data");

// Add event handler
// Handle static file request
app.get("*", (request, response) => {
    console.log(request.url);
    let url = request.url;
    let type = url.slice(url.lastIndexOf(".")+1);
    let contentType = "text/html";
    let filePath = "./frontend"+url;

    if (type === "js") {
        contentType = "application/javascript";
    }
    
    if(type === "css") {
        contentType = "text/css";
    }

    fs.readFile(filePath, function (error, pgResp) {
        if (error) {
            response.writeHead(404);
            response.write('Contents you are looking are Not Found');
        } else {
            response.writeHead(200, { 'Content-Type': contentType });
            response.write(pgResp);
        }
         
        response.end();
    });
    // response.end();
})

// Restful API for users controlling
// Get all users
app.get("/api/users", (request, response) => {
    let users = userController.getUsers();
    let userObjs = users.map((item) => {
        return utils.toJSON(item);
    });
    utils.successResponse(response, {
        data: userObjs
    }, 200);
});

// Get user by id
app.get("/api/users/:id", (request, response) => {
    try{
        let user = userController.getUserById(request.params.id);
        utils.successResponse(response, {
            data: utils.toJSON(user)
        }, 200);
    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
    
});

// Create user
app.post("/api/users", (request, response) => {
    let userObj = request.body;
    let addedUser = new User(userObj.name, userObj.gender, userObj.age);
    let user = userController.addUser(addedUser);
    utils.successResponse(response, {
        data: utils.toJSON(user)
    }, 200);
});

// Update a specific user feature
app.put("/api/users/:id/:feature", (request, response) => {
    try{
        let params = request.params;
        let feature = {};
        if (request.body === undefined || request.body.value === undefined) {
            throw new URLError("Update user failed", "Invalid request body");
        }

        feature[params.feature] = request.body.value;
        let user = userController.updateUserById(params.id, feature);
        utils.successResponse(response, {
            data: utils.toJSON(user)
        }, 200);

    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
    
});

// Update user features
app.put("/api/users/:id", (request, response) => {
    try{
        
        if (request.body === undefined) {
            throw new URLError("Update user failed", "Invalid request body");
        }

        let user = userController.updateUserById(request.params.id, request.body);
        utils.successResponse(response, {
            data: utils.toJSON(user)
        }, 200);

    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
    
});

// Delete user
app.delete("/api/users/:id", (request, response) => {
    try {
        userController.deleteUserById(request.params.id);
        utils.successResponse(response, {
            message: 'Successfully deleted'
        }, 200)
    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
});

// Restful API for cars controlling
// Get all cars
app.get("/api/cars", (request, response) => {
    let cars = carController.getCars();
    let carObjs = cars.map((item) => {
        return utils.toJSON(item);
    });
    utils.successResponse(response, {
        data: carObjs
    }, 200);
    // response.write("hello");
});

// Get cars by id
app.get("/api/cars/:id", (request, response) => {
    try{
        let car = carController.getCarById(request.params.id);
        utils.successResponse(response, {
            data: utils.toJSON(car)
        }, 200);
    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
});

// Create car
app.post("/api/cars", (request, response) => {
    let carObj = request.body;
    let addedCar = new Car(carObj.model, carObj.engine, carObj.infotainmentSystem, carObj.interiorDesign, carObj.currentLocation);
    let car = carController.addCar(addedCar);
    utils.successResponse(response, {
        data: utils.toJSON(car)
    }, 200);
});

// Update a specific car feature
app.put("/api/cars/:id/:feature", (request, response) => {
    try{
        let params = request.params;
        let feature = {};
        if (request.body === undefined || request.body.value === undefined) {
            throw new URLError("Update car failed", "Invalid request body");
        }

        feature[params.feature] = request.body.value;
        let car = carController.updateCarById(params.id, feature);
        utils.successResponse(response, {
            data: utils.toJSON(car)
        }, 200);

    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
});

// Update car features
app.put("/api/cars/:id", (request, response) => {
    try{
        
        if (request.body === undefined) {
            throw new URLError("Update car failed", "Invalid request body");
        }

        let car = carController.updateCarById(request.params.id, request.body);
        utils.successResponse(response, {
            data: utils.toJSON(car)
        }, 200);

    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
    
});

// Delete cars
app.delete("/api/cars/:id", (request, response) => {
    try {
        carController.deleteCarById(request.params.id);
        utils.successResponse(response, {
            message: 'Successfully deleted'
        }, 200)
    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
});

// Restful API for demands controlling
// Get all demands
app.get("/api/demands", (request, response) => {
    let demands = demandController.getDemands();
    let demandObjs = demands.map((item) => {
        return utils.toJSON(item);
    });
    utils.successResponse(response, {
        data: demandObjs
    }, 200);
});

// Get demands by id
app.get("/api/demands/:id", (request, response) => {
    try{
        let demand = demandController.getDemandById(request.params.id);
        utils.successResponse(response, {
            data: utils.toJSON(demand)
        }, 200);
    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
});

// // Create demands
app.post("/api/demands", (request, response) => {
    let demandObj = request.body;
    let addedDemand = new Demand(demandObj.userId, 
                               demandObj.pickUpLocation, 
                               demandObj.dropOffLocation,
                               demandObj.earliestPickUpTime,
                               demandObj.latestDropOffTime,
                               demandObj.desiredCarFeatures);
    let demand = demandController.addDemand(addedDemand);
    utils.successResponse(response, {
        data: utils.toJSON(demand)
    }, 200);
});

// Update a specific demand feature
app.put("/api/demands/:id/:feature", (request, response) => {
    try{
        let params = request.params;
        let feature = {};
        if (request.body === undefined || request.body.value === undefined) {
            throw new URLError("Update demand failed", "Invalid request body");
        }

        feature[params.feature] = request.body.value;

        let demand = demandController.updateDemandById(params.id, feature);
        utils.successResponse(response, {
            data: utils.toJSON(demand)
        }, 200);

    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
});

// Update demand features
app.put("/api/demands/:id/", (request, response) => {
    try{

        if (request.body === undefined) {
            throw new URLError("Update demand failed", "Invalid request body");
        }

        let demand = demandController.updateDemandById(request.params.id, request.body);
        utils.successResponse(response, {
            data: utils.toJSON(demand)
        }, 200);

    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
});

// Delete demand
app.delete("/api/demands/:id", (request, response) => {
    try {
        demandController.deleteDemandById(request.params.id);
        utils.successResponse(response, {
            message: 'Successfully deleted'
        }, 200)
    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
});

// Get schedules
app.get("api/schedules", (request, response) => {
    try {
        let schedules = scheduleService.getCurrentSchedule(0);
        utils.successResponse(response, {
            data: utils.toJSON(schedules)
        })
    } catch (error) {
        utils.errorResponse(response, error, 404);
    }
})

// Start server
const port = process.env.PORT || 8888;
app.listen(port, () => {
    console.log(`Listening on port ${port}...`)
});


const WebSocketServer = require('websocket').server;
const dashboard = require("./controller/DashboardService").getInstance();

const wsServer = new WebSocketServer({
  httpServer: app.Server
});

// WebSocket server
wsServer.on('request', function(request) {
    console.log("Websocket connected...");
    var connection = request.accept(null, request.origin);

    
    connection.on('message', (message) => {

        // Handle messages from users
        if (message.type === 'utf8') {

            // process WebSocket message
            dashboard.getData((data) => {

                // Send data back to user client
                connection.sendUTF(JSON.stringify({
                    data: data
                }));
            });
        }
    });

    connection.on('close', (connection) => {
        // close user connection
        console.log("Websocket closed...");
        dashboard.stop()
    });
});


module.exports.server = app.Server;